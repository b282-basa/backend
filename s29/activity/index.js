
async function findName(db) {
	return await(

		db.users.find(
			{$or: [{firstName: {$regex: 's', $options: 'i'}},
					{lastName: {$regex: 'd', $options: 'i'}}]
			},
			{firstName: 1, lastName:1, _id:0}
			);

};


async function findDeptAge(db) {
	return await (


			db.users.find(
				{$and: [{department:"HR"}, {age: {$gte:70}}]}
			);
		);

};



async function findNameAge(db) {
	return await (


			db.users.find(
				{$and: [{lastName: {$regex: 'e', $options: 'i'}},
						{age: {$lte:30}}]
				},
				{firstName: 1, lastName:1, age: 1, "contact.email":1, department:1}
			);

		);
};


try{
    module.exports = {
        findName,
        findDeptAge,
        findNameAge
    };
} catch(err){

};
