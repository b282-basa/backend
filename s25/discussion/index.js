console.log('Hello B282');

/*
	JSON
	-stands for JavaScript Object Notation
	-is also used in other programming languages
	-Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript Objects
	
	Syntax:
	{
		"propertyA":"valueA",
		"propertyB":"valueB"
	};

	JSON are wrapped in curly braces
	Properties and values are wrapped in double quotes

*/
	
	let sample1 = `

		{
			"name":"Mochi",
			"age":20,
			"address":{
				"city":"Tokyo",
				"country":"Japan"
			}
		}
	`;

	console.log(sample1);

	//MA (3 mins. 6:05PM)
	//create a variable that will hold a "JSON" and create a person object
	//log the variable and send ss

	let person = `

		{
			"name":"Cee",
			"age":23,
			"address":{
				"city":"Quezon City",
				"country":"Philippines"
			}
		}
	`;

	console.log(person);
	console.log(typeof person);//string

	//Are we able to turn a JSON into a JS Object?
	//JSON.parse() - will return the JSON as an object

	console.log(JSON.parse(sample1));
	console.log(JSON.parse(person));

	//JSON Array
	//JSON Array is an array of JSON

	let sampleArr = `

		[
			{
				"email":"mochi@gmail.com",
				"password":"mochimochi",
				"isAdmin":false
			},

			{
				"email":"zenitsu@proton.me",
				"password":"agatsuma",
				"isAdmin":true
			},

			{
				"email":"jsonv@gmail.com",
				"password":"friday13",
				"isAdmin":false
			}
		]
	`;

	console.log(sampleArr);
	console.log(typeof sampleArr);

	//Can we use Array methods on a JSON Array? (sampleArr)
	//No. Because a JSON is a string

	//So what can we do to be able to add more items/objects into our sampleArr?
	//PARSE the JSON array for us to use Array Methods and save it in the variable

	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);//
	console.log(typeof parsedSampleArr);// 

	//can we now delete the last item in the JSON array?
	//Yes
	//What method should we use?
	//pop()

	console.log(parsedSampleArr.pop());
	console.log(parsedSampleArr);

	//if ofr example, we need to send this data back to our client/Front End, it should be in JSON Format

	//JSON.stringify() - this will stringify JS object as JSON
	//JSON.parse() - does not mutate or upate the original JSON
	//therefore, we can actually turn a JS object into a JSON

	sampleArr = JSON.stringify(parsedSampleArr);
	console.log(sampleArr);

	//Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON (frontend/client)

	/*Mini Activity (6 min. 6:47PM)
		1. Given the JSON array, process it and convert to a JS object so we can manipulate the array
		2. Delete the last item in the array and add a new item in the array
		3. Stringify the array back in JSON
		4. Update the jsonArr with the stringi
		fied array
		5. log the jsonArr and send an ss
	*/

	let jsonArr = `
		[
			"pizza",
			"hamburger",
			"spaghetti",
			"shanghai",
			"hotdog stick on a pineapple",
			"pancit bihon"
		]
	`;

	//1.
	let parsedJsonArr = JSON.parse(jsonArr);

	//2.
	parsedJsonArr.pop();
	parsedJsonArr.push("Ice Cream!");

	//3 and 4
	jsonArr = JSON.stringify(parsedJsonArr);

	//5.
	console.log(jsonArr);


	//Gather User Details

	let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = prompt("What is your age?");
	let address = {
		city: prompt("From which city do you live in?"),
		country: prompt("From which country does your city address belong to?")
	};

	let otherData = JSON.stringify({

		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	})

	console.log(otherData);

	let sample3 = `

		{
			'name':'Cardo',
			"age":18,
			"address":{
				"city":"Quiapo",
				"country":"Philippines"
			}
		}
	`;

	try{
		console.log(JSON.parse(sample3))
	}
	catch(err){
		console.log("This will result to an error. Use double quotes for properties and string values")
		console.log(err)
	}finally{
		console.log("This will run!")
	}