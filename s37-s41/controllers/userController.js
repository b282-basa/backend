// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");

const Course = require("../models/Course");
// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user passwords securely.
const bcrypt = require("bcrypt");

const auth = require("../auth");

// Check if email already exists
/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};


// User registration
/*
Business Logic:
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		/*
		bcrypt.hashSync() - is used to generate a hashed version of the password.

		bcrypt.hashSync(reqBody.password, 10) -  will generate a bcrypt hash of the reqBody.password using 10 rounds of salting.

		Rounds of salting refers to the number of iterations or computational rounds used during the hashing process to generate a password hash. 

		Salting is a technique used in password hashing to add an additional random value (known as a salt) to each password before hashing it. 

		In bcrypt, the maximum number of salt rounds that can be applied is 31. 
		The recommended range for the number of salt rounds in bcrypt is typically between 10 and 12.
		*/
		password : bcrypt.hashSync(reqBody.password, 10)
	});
	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if (error) {
			return false;
		// User registration successful
		} else {
			return true;
		};
	});
};

// User authentication
/*
Business Logic:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		// User exists
		} else {
			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the passwords match/result of the above code is true
			if (isPasswordCorrect) {
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return { access : auth.createAccessToken(result) }
			// Passwords do not match
			} else {
				return false;
			};
		};
	});
};

// 
module.exports.getProfile = (data) => {
return User.findById(data.userId).then(result => {

	// Changes the value of the user's password to an empty string when returned to the frontend
	// Not doing so will expose the user's password which will also not be needed in other parts of our application
	// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
	result.password = "";

	// Returns the user information with the password as an empty string
	return result;

	});

};

// S38 Activity
// Retrieve user details
module.exports.getProfile = (data) => {
return User.findById(data.userId).then(result => {
	// Changes the value of the user's password to an empty string when returned to the frontend
	// Not doing so will expose the user's password which will also not be needed in other parts of our application
	// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
	result.password = "";
	// Returns the user information with the password as an empty string
	return result;
	});
};
// S38 Activity End

// JULY 5, 2023

// Controllers for enrolling a user in a course

// Controllers for user enrollment
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});


	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	};
};
