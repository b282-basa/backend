const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();

// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://marixbasa:cute@wdc028-course-booking.aun96z2.mongodb.net/CourseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Allows all resources to access our backend application
app.use(cors());

// Defines the "/users" string to be included for all user routes defined in the "user" route file
// http://localhost:4000/users
app.use("/users", userRoute);
// Defines the "/courses" string to be included for all course routes defined in the "course" route file
// http://localhost:4000/courses
app.use("/courses", courseRoute);

// "process.env.PORT" is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));
