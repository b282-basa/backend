const express = require("express");

const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");


// Route for creating course
/*router.post("/create", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});*/

// S39 Activity Start
router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});
// S39 Activity End



// Route for retrieving all the courses

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving active courses

router.get("/active", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

//DITO TAU NAGSTOP!!!

// Route for retrieve specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// =====================================
// s40 Activity

/*router.put("/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});*/

// CORRECT ANSWER S40 ACTIVITY
//Route for archiving a course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});




module.exports = router;