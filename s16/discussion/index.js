//console.log("Hello World!");

//[SECTION] Arithmetic Operators
// Arithmetic Operators allow mathematical operations


let x = 4, y = 12;

let sum = x + y;
console.log("Result of addition operator: " + sum);
//console.log("Result of addition operator: " + (x+y));

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of product operator: " + product);

let quotient = x / y;
console.log("Result of quotient operator: " + quotient);

let remainder = x % y;
console.log("Result of remainder operator: " + remainder);


//[SECTION] Assignment Operator
//Basic Assignment Operator (=)
//The assignment operator assigns the value of the right operand to a variable

let assignmentNumber = 8;
console.log(assignmentNumber); 

//Addition Assignment Operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.


//assignmentNumber += 2; 
assignmentNumber = assignmentNumber + 2
console.log("Result of addition assignment operator: " + assignmentNumber);


assignmentNumber +=2
console.log("Result of addition assignment operator: " + assignmentNumber);


// MULTIPLE OPERATORS and PARENTHESIS

/* 
MDAS
M - Multiplication 3*4 = 12
D - Division 12/5 = 2.4
A - Addition 1+2 = 3
S - Subtraction 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation" + mdas); 0.6

/*
P - Parenthesis
E - Exponent
M - Multiplication 
D - Division 
A - Addition
S - Subtraction
*/

/*
1. 4/5 = 0.8
2. 2-3 = -1
3. -1 * 0.8 = -0.8
4. 1 + -0.8 = 0.2
*/
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);


// Increment (++) and Decrement (--)
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decremet was applied to

let z = 1;

let increment = z++;
console.log("Result of increment:" + z);

let decrement = z--;
console.log("Result of decrement:" + z);

// [SECTION] Type Coercion
// type coercion is the automatic or implicit conversion of values from one data type to another
// Values are automatically converted from one data type to another in order to resolve operations

let numA = '10', numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);


// [SECTION] Comparison Operators
// Equality Operator (==)
// Checks whether the operands are equal/have the same content
console.log ("Equality Operator")
console.log (1==1); // true
console.log (1=='1') // true
console.log ('juan'=="juan") // true
console.log (0 == false) // 0 = false (answer true)


// Strict Equality Operator (===)
// Checks whether the operands are equal/have the same content and also compares the data types of the two values.
console.log ("Strict Equality Operator")
console.log (1===1); // T
console.log (1==='1') // F
console.log ('juan'==="juan") // T 
console.log (0 === false) // F

// Inequality Operator (!=)
// checks whether the operands are NOT equal/have different content
console.log ("InEquality Operator")
console.log (1!=1); // F
console.log (1!='1') // F
console.log ('juan'!="juan") // F
console.log (0 != false) // F


// Strict InEquality Operator (!==)
// Checks whether the operands are NOT equal/have the different content and also compares the data types of the two values.
console.log ("Strict InEquality Operator")
console.log (1!==1); // F
console.log (1!=='1') // T
console.log ('juan'!=="juan") // F 
console.log (0 !== false) // T

// [SECTION] Relational Operations
let a = 50, b = 65;

// GT or Greater Than Operator ( > )
let isGreaterThan = a > b; // F

// LT or Less Than Operator ( < )
let isLessThan = a < b; // T

// GTE or Greater Than or Equal Operator ( >= )
let isGTorGTE = a >= b; // F

// LTW or Less Than or Equal Operator ( <= )
let isLTorLTE = a <= b; // T

console.log("Relational Operations");
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorGTE);
console.log(isLTorLTE);

// [SECTION] Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&&)
// Returns true if all operands  are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);  // result: false

// Logical OR Operator
// Returns TRUE if one of the operands is True

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet); // result: true

//Logical NOT Operator
// Returns opposite value
let someRequirementsMet = !isRegistered;
console.log("Result of Logical NOT Operator: " + someRequirementsMet); // result: true