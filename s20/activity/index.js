// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for (ctr=0; ctr< string.length; ctr++)
{
    if (
        string[ctr].toLowerCase() == "a" ||
        string[ctr].toLowerCase() == "e" ||
        string[ctr].toLowerCase() == "i" ||
        string[ctr].toLowerCase() == "o" ||
        string[ctr].toLowerCase() == "u"
        )
    {
        continue;
       
    }

    else 
    {
         filteredString += string[ctr];
    }


}
 console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}