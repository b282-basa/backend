

/*
SYNTAX: 
	while(expression/condition)
	{
	statement
	}
*/

let count = 5;

//while the value o count is not equal to zero
while (count !== 0) 
{
	//the curret value of count is printed out
	console.log("While: " + count);
	//decrease the value by 1 after every iteration to STOP the loop when t reaches 0
	count--; 
};


// [SECTION] DO WHILE LOOP

// A do-while loop work a lot like while loop. But unline while loops, do while loops guarantee that the code will be execute at least once

/*
SYNTAX :
	do {
		statemtn
	} while (expresion/condition)
*/


// Number , when cancel, it returns and stores 0 (zero)
// ParseInt, when cancel, it returns NaN

/*let number = Number(prompt("Give me a number:"));

do
{
	// the current value of number is printed out
	console.log("Do While: " + number);
	// increase the value of numbe 1 after every iteration to stop the loop when it reaches 10 or greater than 10
	number+=1;
}
//providing a number of 10 or greater will run the code block once and will stop the loop
while (number < 10)*/


// [SECTION] FOR LOOP

/* 
SYNTAX
	for (initialization; expression/condition; finalExpression)
	{
		statement;
	}

	// initialization - start
	// expression/condition - end
	// finalexpression - counter
*/


for (let count=0; count <=20; count++)
{
	// the currrent value of count is printed out
	console.log(count)
};


// .length property
// characters in strin may be counted using the .length property

// the first character in a string correponds to the number 0, the next is 1 up to the nth number
let myString = "alex" 
/*
	a-index 0
	l-index 1
	e-index 2
	x-index 3
*/
console.log("Result of .length property: " + myString.length);

//Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// Loop that will print out individual letters using for loop

for (let ctr = 0 ; ctr < myString.length; ctr++)
{
	console.log(myString[ctr]);
}

// Loop that will print out the letters of the name individually and print out the number 3 instead whe the letter to be printed out is a vowel
console.log("++++++++++++++++++");

let myName = "AlEx";
for (let i = 0; i < myName.length; i++)
{
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		)
	{
		console.log(3);
	}
	else
	{
		console.log(myName[i]);
	}

};


// [SECTION] Continue and Break Statements

// The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

// The "break" statement is used to terminate the current loop once a match has been found

for (let count = 0; count <=20; count++)
{
	// if remainder is equal to 0 
	if (count % 2 === 0)
	{
		//tells to continue to the next iteration of the loop
		// this ignores all statements located after the continue statement
		continue;
	};

	console.log("Continue and Break: " + count);

	if (count > 10)
	{
		// tells the code to terminate/stop loop even if the expresson/condition of the loop defines that should execute so long as the value of count is that or equal to 20
		// number values after 10 will no longer printed.
		break;
	};
};


// Loop that will iterate based on the length of the string
// If the vowel is equal to a, continue to the next iteration of the loop
// If the current letter is equal to r, stop the loop

let name = "alexandro";

for (let i = 0; i < name.length; i++)
{
	// If the vowel is equal to a, continue to the next iteration of the loop
	if (name[i].toLowerCase() === "a")
	{
		console.log("Continue to the next Iteration");
		continue;
	}

	// the current letter is printed out based on its index 
	console.log(name[i]);

	// if the current letter is equal to "r", stop the loop
	if (name[i].toLowerCase() === "r" )
	{
		break;
	}
}