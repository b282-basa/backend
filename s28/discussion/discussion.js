// CRUD Operations

// CREATE OPERATION

// in plural form
// insertOne() - inserts one document to the collection

db.users.insertOne({
	"firstname": "John",
	"lastname": "Smith",
});

// insertMay() - Inserts multiple documents to the collection

db.users.insertMany([
	{"firstname": "John", "lastname": "Doe"},
	{"firstname": "Jane", "lastname": "Doe"},

]);

// READ OPERATION
// find() - get all the inserted users

db.users.find();

// RETRIEVING SPECIFIC DOCUMENTS

db.users.find({ "lastname" : "Doe"});

// UPDATE OPERATION
// updateOne() - modify one document (or record)

db.users.updateOne(
	{	
		"_id": ObjectId("648af8e3ff0183a8bbb8b9e0")
	},
	{
		$set: 
		{
			"email": "johnsmith@gmail.com"
		}
	}
);

// updateMany() - modify multiple documents

db.users.updateMany(
	{
		"lastname": "Doe"
	},
	{
		$set:
		{
			"isAdmin": false
		}
	}
);

// DELETE OPERATION
// deleteMany - deletes multiple documents

db.users.deleteMany({"lastname": "Doe"});

// deleteOne - deletes single document

db.users.deleteOne({ "_id": ObjectId("648af8e3ff0183a8bbb8b9e0")});