// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


//Using the aggregrate method

/*
SYNTAX:

db.collectionName.aggregate([
  {$match: {field: valueA}}, 
  {$group: {_id: "$fieldB"}, {result: {operation}}
]);

*/


// $ symbol will refer to a field name that is available in the documents that are being aggregated on


//  $ symbol will refer to a field name that is available in the documents that are being aggregated on


db.fruits.aggregate([
  // $match is used to pass documents that meet specified condition(s) to the next pipeline stage/aggregration process
  {$match: {onSale: true}},

  // $group is used to group elements together and field-value pairs using the data from the grouped elements
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);


// Field projection with aggregation
/*
SYNTAX:
  {$project : {field: 1/0}}
*/

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
  // "$project" can be used when aggregating data to include/exclude fields from the returned results
  {$project : {_id: 0}}
]);

// Sorting aggregated result
/*
SYNTAX:
  {$sort : {field: 1/-1}}
*/
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
  // "$sort" can be used to change the order of aggregated results
  {$sort : {total: 1}}
]);


// Aggregating results based on array fields
/*
SYNTAX:
  {$unwind: field}
*/

db.fruits.aggregate([
  // $unwind deconstructs an array field from a collection with an array value to output a result for each element
  {$unwind: "$origin"}
]);


// Displays fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
  {$unwind: "$origin"},
  {$group: {_id: "$origin", kind_of_fruits: {$sum: 1}}}
]);
