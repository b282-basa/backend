db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


async function fruitsOnSale(db) {
	return await(

			db.fruits.aggregate([
        {$match: {onSale: true}},
        {$count: "fruitsOnSale"}
        ]);
		);
};



async function fruitsInStock(db) {
	return await(


      db.fruits.aggregate([
        {$match: {stock: {$gte: 20}}},
        {$count: "enoughStock"}
        ]);
      }
		);
};


async function fruitsAvePrice(db) {
	return await(

			
      db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: 
                {_id: "$supplier_id",
                avg_price: {$avg: "$price"}}},
        {$sort: {avg_price: -1}}
        ]);
		);
};



async function fruitsHighPrice(db) {
	return await(

			
      db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: 
                {_id: "$supplier_id",
                max_price: {$max: "$price"}}},
        {$sort: {max_price: 1}}
        ]);
		);
};




async function fruitsLowPrice(db) {
	return await(

			
      db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: 
                {_id: "$supplier_id",
                min_price: {$min: "$price"}}},
        {$sort: {min_price: -1}}
        ]);
		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
