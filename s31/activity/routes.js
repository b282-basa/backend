//Add solution here
let http = require("http");

const port = 3000;

const successRequestNumber = 200;

const app = http.createServer((request, response) => {
	
	if(request.url == '/login')
	{
		response.writeHead(successRequestNumber, {'Content-Type': 'text/plain'});
		response.end('Welcome to the login page!');
	}
	else
	{
		response.writeHead(successRequestNumber, {'Content-Type': 'text/plain'});
		response.end('I\'m sorry the page you are looking for cannot be found');
	}
});

app.listen(port);

console.log(`The server is successfully running`);






















//Do not modify
module.exports = {app}