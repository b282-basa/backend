
// use the 'require' directive to load tha express module/package
const express = require("express");


// Create an application using express
// "app" is our server
const app = express();

const port = 3000;

// Methods used from express.js are middleware
// Middleware is software that provides services  and  capabilities to applications outside of what's offered by the opering system


//allows your app to read json data
app.use(express.json());

//allows your app to read data from any forms
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
// GET

app.get("/greet", (request, response) => {
	
	// "response.send" method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
	});


// POST

app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple Registration

let users = [
	{
		"username": "Marix",
		"password": "marix23"
	},

	{
		"username": "Rb",
		"password": "rb31"
	},

	{
		"username": "Fuffer",
		"password": "fuffer2331"
	},

	{
		"username": "Kutchie",
		"password": "kutchiecutie"
	},

	{
		"username": "Ferffa",
		"password": "ferffacutie"
	}
	];

//let users = [];

app.post("/signup", (request, response) => {
	 if (request.body.username !== "" && request.body.password !== "")
	 {
	 	users.push(request.body);

	 	response.send(`User ${request.body.username} successfully registered!`);
	 }
	 else {
	 	response.send("Please input BOTH username and password");
	 }
});


// [SECTION] ACTIVITY AREA



// [1] DONE
// get home
app.get("/home", (request, response) => {

	response.send("Welcome to the home page.")
	});

// [2] DONE
// get users
app.get("/users", (request, response) => {

	response.send(users);
	});


// [3]
// delete user



app.delete("/delete-user", (request, response) => {

	

if (users != "")
{
	for (let index=0; index <users.length; index++)
	{
		if (users[index].username == request.body.username)
		{
			users.splice(index, 1);
			
			// checker if the user is already removed inside the array
			//console.log(users); //successfully removed a user in the in the array via console

			response.send(`User ${request.body.username} has been deleted`)

			//use the GET USERS to check if the array is missing one user 
			break;

			//OVERTHINK MALALA!!!
		}
		

		if (users.username == undefined)
		{
			response.send(`User does not exist.`);
		}
	}

}
else
{
	response.send(`No users found`);
}

})


app.listen(port, () => console.log(`Server running at port ${port}`));


