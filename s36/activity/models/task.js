
const mongoose = require("mongoose");
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
});

//"module.exports" is a way for Nods JS to treeat this value as a "package" that can be used by other files.
module.exports = mongoose.model("Task", taskSchema);