// BASTA eto ung mga procedures or utos naten

// Contains instructions on HOW your API will perform its intended tasks

// All the operations it can do will beplaced in this file


const Task = require("../models/task");


// Controller for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

// Controller creating a tasks

module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
			name: requestBody.name
		});

	return newTask.save().then((task, error) => {
		if (error)
		{
			console.log(error);
			return false; // for sure na di magrurun ung false statement, there's a possibility na mag run pa ung code.
		}

		else
		{
			return task;
		}
	});

};


// Controller deleting a task

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>
	{
		if (err)
		{
			console.log(err);
			return false;
		}
		else
		{
			return "Deleted Task."
		};

	});
};

// Controller updating a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,err) =>
	{
		if (err)
		{
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => 
		{
			if (err)
			{
				console.log(saveErr);
				return false;
			}
			else
			{
				return "Task Updated."
			};
		
		});
	});
};

// Controller for getting specific the tasks

module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then((result) => {
			return result;
		
	});
};


// Controller for updating a task

module.exports.updateTask = (taskId, newStatus) => {
	return Task.findById(taskId).then((result,err) =>
	{
		if (err)
		{
			console.log(err);
			return false;
		}

		result.status = newStatus.status;

		return result.save().then((updatedTask, saveErr) => 
		{
			if (err)
			{
				console.log(saveErr);
				return false;
			}
			else
			{
				return result;
			};
		
		});
	});
};