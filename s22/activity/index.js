/*
    Create functions which can manipulate our arrays.
*/

/*
    Important note: Don't pass the arrays as an argument to the function. 
    The functions must be able to manipulate the current given arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    
    console.log(registeredUsers);

    function register(username="User")
    {
        let checkRegUsers = registeredUsers.includes(username);

        if (checkRegUsers) //true
        {
            return console.log(`Registration Failed. Username already exists.`)
        }
        else //false
        {
            registeredUsers.push(username);
            return console.log(registeredUsers);
        }
    };

    //register();

/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

    function addFriend(username="User")
    {
        let foundUser = registeredUsers.includes(username);

        if (foundUser) // true
        {
          
            let foundFriend = friendsList.includes(username);

            if (foundUser != foundFriend)
            {
                friendsList.push(username);
                return console.log(`You have added ${username} as a friend!`);
            }
            else if (foundUser == foundFriend)
            {

                return console.log(`Friend is already in the friend list.`);
            }

        }
        else //false
        {
            return console.log(`User not found.`);
        }

    };

    //addFriend();
/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    
    function displayFriends()
    {
        if (friendsList.length > 0)
        {
            friendsList.forEach(function(friend)
            {
                return console.log(friend);
            });
        } 
        else
        {
             return console.log(`You have 0 friends. Add one first`);
        }
    };

/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

    function displayNumberofFriends()
    {
        if (friendsList.length > 0)
        {
            return console.log(`You currently have ${friendsList.length} friend/s.`)
        }
        else if (friendsList.length == 0)
        {
            return console.log(`You currently have ${friendsList.length} friend. Add one first `);
        }
    };

/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
    function deleteFriend()
    {
        if (friendsList.length == 0)
        {
            return console.log(`You currently have ${friendsList.length} friend.`)
        }
        else if (friendsList.length > 0)
        {
            friendsList.pop()
            //return console.log(`The current friendslist after deletion is/are:`);
            return console.log(friendsList);
            
        }

    };


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/


    function deleteFriendManualSelection(username)
    {
        if (friendsList.length> 0)
        {
            let deletedFriendIndex = friendsList.indexOf(username);
           
            // friendsList.splice(friendsList.indexOf(username), 1)
            if (deletedFriendIndex == -1)
            {
                return console.log(`Friend is not found in the friends list.`);

            }
            else
            {
                friendsList.splice(deletedFriendIndex, 1);
                return console.log(friendsList);
            }
           
        }
        else if (friendsList.length == 0)
        {
            return console.log(`You currently have ${friendsList.length} friend.`)
        }
    };



//For exporting to test.js
try{
    module.exports = {

        registeredUsers: typeof registeredUsers !== 'undefined' ? registeredUsers : null,
        friendsList: typeof friendsList !== 'undefined' ? friendsList : null,
        register: typeof register !== 'undefined' ? register : null,
        addFriend: typeof addFriend !== 'undefined' ? addFriend : null,
        displayFriends: typeof displayFriends !== 'undefined' ? displayFriends : null,
        displayNumberOfFriends: typeof displayNumberOfFriends !== 'undefined' ? displayNumberOfFriends : null,
        deleteFriend: typeof deleteFriend !== 'undefined' ? deleteFriend : null

    }
} catch(err){

}