//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator


const getCube = 2 ** 3;


// Template Literals


console.log(`The cube of 2 is ${getCube} .`);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;


//8. 
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}


//9. 

const {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

//10.

numbers.forEach((number) => { console.log(`${number}`); });

//11.

//REDUCE(): https://www.freecodecamp.org/news/reduce-f47a7da511a9/

let reduceNumber = numbers.reduce(function(first,last) { return first+last;});
console.log(reduceNumber);

// Javascript Classes

//12.

class Dog 
{
    constructor(name, age, breed)
    {
        this.name = name;
        this.age = age;
        this.breed = breed;
    };
};

//13.

const myNewDog = new Dog("Fuffer", 4, "Siberian Husky");
console.log(myNewDog);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}