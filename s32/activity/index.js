let http = require("http");

const app = http.createServer(function (request, response) {

    //Add code here

    let number = 200;

    // GET
    if (request.url == "/" && request.method == "GET")
    {
        response.writeHead(number, {'Content-Type': 'text/plain'});
        response.end('Welcome to Booking System');
    }

    //GET
    if (request.url == "/profile" && request.method == "GET")
    {
        response.writeHead(number, {'Content-Type': 'text/plain'});
        response.end('Welcome to your profile!');        
    }

    //GET
    if (request.url == "/courses" && request.method == "GET")
    {
        response.writeHead(number, {'Content-Type': 'text/plain'});
        response.end('Here\'s our courses available');        
    }

    //POST
    if (request.url == "/addCourses" && request.method == "POST")
    {
        response.writeHead(number, {'Content-Type': 'text/plain'});
        response.end('Add a course to our resources.');        
    }

    //PUT
    if (request.url == "/updateCourses" && request.method == "PUT")
    {
        response.writeHead(number, {'Content-Type': 'text/plain'});
        response.end('Update a course to our resources.');        
    }

     //DELETE
    if (request.url == "/archiveCourses" && request.method == "DELETE")
    {
        response.writeHead(number, {'Content-Type': 'text/plain'});
        response.end('Archive course to our resources.');        
    }

})


















//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;