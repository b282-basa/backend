console.log("Hello Unibars!");

// Functions

function printInput()
{
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}


// Parameters and Arguments

// (name) - is called a PARAMETER, act as variable or container that is existing in the function
// a "PARAMETER" acts as a named variable or container that exists only inside a function

// let name = "Juana";
function printName(name)
{
	console.log("My name is " + name);
}

// ("Juana") - the information or data provided directly into the function is called an ARGUMENT
printName("Juana");
printName("John");
printName("Jane");

//Variables can also be passed as an argument

let sampleVariable = "Yui";

printName(sampleVariable);

function checkDivisibilityBy8(num)
{
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is "  + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(65);
checkDivisibilityBy8(28);

//Function as Arguments
function argumentFunction() 
{
	console.log("This function was passed as an argument before the message was printed.");
};

function invokeFunction(argumentFunction)
{
	argumentFunction();
};

invokeFunction(argumentFunction);
console.log(argumentFunction);


//Multiple "arguments" will correspond to the number "parameters" declared in a function in succeeding order

function createFullName(firstName, middleName, lastName)
{
	console.log(firstName + " " + middleName + " " + lastName);
};

// "Juan" will be stored in the parameter "firstname"
// "Dela" will be stored in the parameter "middlename"
// "Cruz" will be stored in the parameter "lastname"
createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Dela'); // undefined lastname
createFullName('Juan', 'Dela', 'Cruz', 'Hello'); // Hello will not read 


// Using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);


// 2nd try
function createFullName(middleName, firstName, lastName)
{
	console.log(firstName + " " + middleName + " " + lastName);
};

createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Dela'); // undefined lastname
createFullName('Juan', 'Dela', 'Cruz', 'Hello'); // Hello will not read 

// RESULT : Dela Juan Cruz


